package pluradj.janusgraph.example;

import java.util.List;
import java.util.Map;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;
import org.janusgraph.core.attribute.Geo;
import org.janusgraph.core.attribute.Geoshape;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JavaExample {
	private static final Logger LOGGER = LoggerFactory.getLogger(JavaExample.class);

	public static void main(String[] args) {
        JanusGraph graph = JanusGraphFactory.open("conf/janusgraph-cassandra-es.properties");
        graph.tx().commit();
        GraphTraversalSource g = graph.traversal();
        if (g.V().count().next() == 0) {
            // load the schema and graph data
            MyGraph.load(graph);
        }
        Map<String, ?> saturnProps = g.V().has("name2", "hadi").valueMap(true).next();
        GraphTraversal<Vertex, Map<String, Object>> valueMap = g.V().has("name2", "hadi").out("office").valueMap(true);
        while (valueMap.hasNext()) {
        	 Map<String, Object> next = valueMap.next();
        	 LOGGER.debug(next.toString());        	
        }
        
        GraphTraversal<Vertex, Map<String, Object>> valueMap2 = g.V().has("name2", "salman").out("friend").out("office").valueMap(true);
        while (valueMap2.hasNext()) {
             Map<String, Object> next = valueMap2.next();
             LOGGER.debug(next.toString());        	
        }
        
        GraphTraversal<Vertex, Map<String, Object>> valueMap3 = g.V().has("name2", "salman").in("boss").valueMap(true);
        while (valueMap3.hasNext()) {
             Map<String, Object> next = valueMap3.next();
             LOGGER.debug(next.toString());        	
        }
       
        LOGGER.info(saturnProps.toString());
        List<Edge> places = g.E().has("place", Geo.geoWithin(Geoshape.circle(37.97, 23.72, 50))).toList();
        LOGGER.info(places.toString());
        System.exit(0);
    }
}
