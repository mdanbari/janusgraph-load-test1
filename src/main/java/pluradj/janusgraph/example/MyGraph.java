// Copyright 2017 JanusGraph Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package pluradj.janusgraph.example;

import org.apache.tinkerpop.gremlin.process.traversal.Order;
import org.apache.tinkerpop.gremlin.structure.Direction;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.T;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.EdgeLabel;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;
import org.janusgraph.core.JanusGraphTransaction;
import org.janusgraph.core.Multiplicity;
import org.janusgraph.core.PropertyKey;
import org.janusgraph.core.attribute.Geoshape;
import org.janusgraph.core.schema.ConsistencyModifier;
import org.janusgraph.core.schema.JanusGraphIndex;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.graphdb.database.StandardJanusGraph;

import com.google.common.base.Preconditions;

/**
 * Example Graph factory that creates a {@link JanusGraph} based on roman mythology.
 * Used in the documentation examples and tutorials.
 *
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */
public class MyGraph {

    public static final String INDEX_NAME = "search";
    private static final String ERR_NO_INDEXING_BACKEND = 
            "The indexing backend with name \"%s\" is not defined. Specify an existing indexing backend or " +
            "use GraphOfTheGodsFactory.loadWithoutMixedIndex(graph,true) to load without the use of an " +
            "indexing backend.";

    public static JanusGraph create(final String directory) {
        JanusGraphFactory.Builder config = JanusGraphFactory.build();
        config.set("storage.backend", "berkeleyje");
        config.set("storage.directory", directory);
        config.set("index." + INDEX_NAME + ".backend", "elasticsearch");

        JanusGraph graph = config.open();
        MyGraph.load(graph);
        return graph;
    }

    public static void loadWithoutMixedIndex(final JanusGraph graph, boolean uniqueNameCompositeIndex) {
        load(graph, null, uniqueNameCompositeIndex);
    }

    public static void load(final JanusGraph graph) {
        load(graph, INDEX_NAME, true);
    }

    private static boolean mixedIndexNullOrExists(StandardJanusGraph graph, String indexName) {
        return indexName == null || graph.getIndexSerializer().containsIndex(indexName) == true;
    }

    public static void load(final JanusGraph graph, String mixedIndexName, boolean uniqueNameCompositeIndex) {
        if (graph instanceof StandardJanusGraph) {
            Preconditions.checkState(mixedIndexNullOrExists((StandardJanusGraph)graph, mixedIndexName), 
                    ERR_NO_INDEXING_BACKEND, mixedIndexName);
        }

        //Create Schema
        JanusGraphManagement mgmt = graph.openManagement();
        final PropertyKey name = mgmt.makePropertyKey("name2").dataType(String.class).make();
        JanusGraphManagement.IndexBuilder nameIndexBuilder = mgmt.buildIndex("name2", Vertex.class).addKey(name);
        if (uniqueNameCompositeIndex)
            nameIndexBuilder.unique();
        JanusGraphIndex namei = nameIndexBuilder.buildCompositeIndex();
        mgmt.setConsistency(namei, ConsistencyModifier.LOCK);
        
        
        final PropertyKey age = mgmt.makePropertyKey("age2").dataType(Integer.class).make();
        if (null != mixedIndexName)
            mgmt.buildIndex("vertices", Vertex.class).addKey(age).buildMixedIndex(mixedIndexName);
        
        final PropertyKey martual_status = mgmt.makePropertyKey("martual_status").dataType(String.class).make();

        mgmt.makeEdgeLabel("lives2").make();
        mgmt.makeEdgeLabel("study").make();
        mgmt.makeEdgeLabel("brothe2").make();
        mgmt.makeEdgeLabel("friend").make();

        
        mgmt.makeVertexLabel("location2").make();
        mgmt.makeVertexLabel("university").make();
        mgmt.makeVertexLabel("office").make();
        mgmt.makeVertexLabel("boss").make();
        mgmt.makeVertexLabel("ex-boss").make();
        mgmt.makeVertexLabel("employee").make();

        mgmt.commit();

        JanusGraphTransaction tx = graph.newTransaction();
        // vertices

        Vertex salman = tx.addVertex(T.label, "boss", "name2", "salman", "age2", 35,"martual_status","married");
        Vertex hadi = tx.addVertex(T.label, "employee", "name2", "hadi", "age2", 32,"martual_status","married");
        Vertex naser = tx.addVertex(T.label, "employee", "name2", "naser", "age2", 32,"martual_status","married");
        Vertex majid = tx.addVertex(T.label, "employee", "name2", "majid", "age2", 28,"martual_status","single");
        Vertex milad = tx.addVertex(T.label, "ex-boss", "name2", "milad", "age2", 31,"martual_status","married");
        Vertex mohammad = tx.addVertex(T.label, "employee", "name2", "mohammad", "age2", 22,"martual_status","single");
        Vertex farhad = tx.addVertex(T.label, "employee", "name2", "farhad", "age2", 25,"martual_status","single");
        
        Vertex amirkabir_uni = tx.addVertex(T.label, "university", "name2", "Amirkabir University");
        Vertex shahed_uni = tx.addVertex(T.label, "university", "name2", "Shahed University");        
        
        Vertex tehran = tx.addVertex(T.label, "location2", "name2", "tehran");
        Vertex eslamshahr = tx.addVertex(T.label, "location2", "name2", "eslamshahr");
        Vertex varamin = tx.addVertex(T.label, "location2", "name2", "varamin");

        
        Vertex ict = tx.addVertex(T.label, "office", "name2", "ICT-CERT");
        Vertex ariban = tx.addVertex(T.label, "office", "name2", "Ariaban");
        Vertex matiran = tx.addVertex(T.label, "office", "name2", "Matiran");
        Vertex mohaymen = tx.addVertex(T.label, "office", "name2", "Mohaymen");   

        // edges        
        
        salman.addEdge("lives2", tehran).inVertex();
        salman.addEdge("brothe2", majid).bothVertices();
        salman.addEdge("friend", milad).bothVertices();
        salman.addEdge("friend", hadi).bothVertices();
        salman.addEdge("study", shahed_uni).inVertex();
        salman.addEdge("office", ict).inVertex();
        salman.addEdge("office", ariban).inVertex();
        salman.addEdge("office", mohaymen).inVertex();

        
        milad.addEdge("lives2", tehran).inVertex();
        milad.addEdge("friend", salman).bothVertices();
        milad.addEdge("friend", hadi).bothVertices();
        milad.addEdge("study", amirkabir_uni).inVertex();
        milad.addEdge("office", ict).inVertex();
        milad.addEdge("office", ariban).inVertex();
        milad.addEdge("office", matiran).inVertex();
        
        hadi.addEdge("lives2", tehran).inVertex();
        hadi.addEdge("friend", milad).bothVertices();
        hadi.addEdge("friend", salman).bothVertices();
        hadi.addEdge("friend", hadi).bothVertices();
        hadi.addEdge("friend", naser).bothVertices();
        hadi.addEdge("friend", farhad).bothVertices();
        hadi.addEdge("friend", mohammad).bothVertices();
        hadi.addEdge("study", shahed_uni).inVertex();
        hadi.addEdge("office", ict).inVertex();
        hadi.addEdge("office", mohaymen).inVertex();
        hadi.addEdge("boss", salman).inVertex();
        hadi.addEdge("ex-boss", milad).inVertex();
        
        majid.addEdge("lives2", tehran).inVertex();
        majid.addEdge("brothe2", milad).bothVertices();
        majid.addEdge("friend", naser).bothVertices();
        majid.addEdge("friend", farhad).bothVertices();
        majid.addEdge("friend", mohammad).bothVertices();
        majid.addEdge("office", ict).inVertex();
        majid.addEdge("office", ariban).inVertex();
        majid.addEdge("boss", salman).inVertex();
        
        naser.addEdge("lives2", tehran).inVertex();
        naser.addEdge("friend", farhad).bothVertices();
        naser.addEdge("friend", mohammad).bothVertices();
        naser.addEdge("office", ict).inVertex();
        naser.addEdge("office", mohaymen).inVertex();
        naser.addEdge("office", ariban).inVertex();
        naser.addEdge("boss", salman).inVertex();
        naser.addEdge("ex-boss", milad).inVertex();
        
        farhad.addEdge("lives2", varamin).inVertex();
        farhad.addEdge("office", mohaymen).inVertex();
        farhad.addEdge("office", ariban).inVertex();
        farhad.addEdge("boss", salman).inVertex();
        farhad.addEdge("friend", mohammad).bothVertices();
        
        mohammad.addEdge("lives2", eslamshahr).inVertex();
        mohammad.addEdge("office", mohaymen).inVertex();
        mohammad.addEdge("office", ariban).inVertex();
        mohammad.addEdge("boss", salman).inVertex();



        // commit the transaction to disk
        tx.commit();
    }


}
